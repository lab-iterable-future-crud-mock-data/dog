import 'package:dog_app/dog.dart';
import 'dog.dart';

var lastId = 3;

var mockDog = [
  Dog(id: 1, name: 'kido', age: 8),
  Dog(id: 2, name: 'meepo', age: 5)
];

int getNewId() {
  return lastId++;
}

Future<void> addNew(Dog dog) {
  return Future.delayed(Duration(seconds: 1), () {
    mockDog.add(Dog(id: getNewId(), name: dog.name, age: dog.age));
  });
}

Future<void> saveDog(Dog dog) {
  return Future.delayed(Duration(seconds: 1), () {
    var index = mockDog.indexWhere((item) => item.id == dog.id);
    mockDog[index] = dog;
  });
}

Future<void> delDog(Dog dog) {
  return Future.delayed(Duration(seconds: 1), () {
    var index = mockDog.indexWhere((item) => item.id == dog.id);
    mockDog.removeAt(index);
  });
}

Future<List<Dog>> getDogs() {
  return Future.delayed(Duration(seconds: 1), () => mockDog);
}
